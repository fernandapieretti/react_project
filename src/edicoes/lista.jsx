import React, {Component} from 'react'
import { hashHistory } from 'react-router';
import Titulo from '../template/titulo'
import TabelaPadrao from '../template/tabela'
import Modal from '../template/modal'
import IconButton from '../template/iconbutton'
import axios from 'axios'

const URL = 'http://localhost:8080/semana/'

export default class Lista extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lista: [],
            show: false,
            id: ''
        }
        this.handleEdit = this.handleEdit.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        this.handleRemove = this.handleRemove.bind(this)
        this.handleConfirmRemove = this.handleConfirmRemove.bind(this)
    }

    componentDidMount() {
        this.refresh()
    }

    refresh() {
        axios.get(`${URL}edicaoController/api`)
            .then(res => this.setState({...this.state, lista: res.data}))
    }

    handleCancel() {
        this.setState({ id: '', show: false})
    }

    handleEdit(edicao) {
        hashHistory.push('edicoes/editar/' + edicao)
    }

    handleConfirmRemove(idRemover) {
        this.setState({ id: idRemover, show: true })
    }

    handleRemove() {
        const id = {
            id: this.state.id
        }
        axios.post(`${URL}edicaoController/apiDelete`, id, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        }).then(resp => { 
            this.refresh()
            this.handleCancel()
        })
    }

    render() {
        return(
            <div className="content-wrapper">
                <div className="container-fluid">

                    <Titulo titulo="Edições" subtitulo="Listagem" url="edicoes" />

                    <TabelaPadrao headers="Id, Título, Num, Ações">
                        <tbody>
                        {
                            this.state.lista.map((edicao, index) => {
                                return(
                                    <tr key={edicao.id}>
                                        <td>{edicao.id}</td>
                                        <td>{edicao.titulo}</td>
                                        <td>{edicao.numero}</td>
                                        <td>
                                            <IconButton type="success" icon="pencil" onClick={() => this.handleEdit(edicao.id)} />
                                            <IconButton type="danger" icon="trash" onClick={() => this.handleConfirmRemove(edicao.id)} />
                                        </td>                                        
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </TabelaPadrao>

                    <Modal show={this.state.show}
                    titulo="Confirmação"
                    texto="Deseja excluir esta edição? A ação não poderá ser desfeita"
                    handleCancel={() => this.handleCancel()}
                    handleConfirm={() => this.handleRemove()} />
                </div>
            </div>
        )
    }
}