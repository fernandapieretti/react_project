import React, {Component} from 'react'
import { hashHistory } from 'react-router';
import Titulo from '../template/titulo'
import InputPadrao from '../template/input'
import AcoesForm from '../template/acoesForm'
import axios from 'axios'

const URL = 'http://localhost:8080/semana/'

export default class Editar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.params.id,
            titulo: '',
            numero: ''
        }
        this.handleChangeTitulo = this.handleChangeTitulo.bind(this)
        this.handleChangeNumero = this.handleChangeNumero.bind(this)
    }

    componentDidMount() {
        axios.get(`${URL}edicaoController/apiBuscarPorId/${this.state.id}`)
        .then(res=> {
            const edicao = res.data
            this.setState({
                titulo: edicao[0].titulo,
                numero: edicao[0].numero
            })
        })
    }

    handleChangeTitulo(e) {
        this.setState({ ...this.state, titulo: e.target.value })
    }

    handleChangeNumero(e) {
        this.setState({ ...this.state, numero: e.target.value })
    }

    handleEdit() {
        const edicao = {
            id: this.state.id,
            titulo: this.state.titulo,
            numero: this.state.numero
        }
        axios.post(`${URL}edicaoController/apiEditar`, edicao, {
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).then(res => {
            hashHistory.push("/edicoes")
        })
    }

    render() {
        return(
            <div className="content-wrapper">
                <div className="container-fluid">
                    <Titulo titulo="Edições" subtitulo="Editar" url="edicoes" hideBtnNovo="true" />

                    <div className="row">
                        <InputPadrao label="Título" id="titulo" type="text" value={this.state.titulo} largura="col-md-12" handleChange={this.handleChangeTitulo} />
                        <InputPadrao label="Número" id="numero" type="text" value={this.state.numero} largura="col-md-3" handleChange={this.handleChangeNumero} />
                        <AcoesForm onClick={() => this.handleEdit()} voltar="/edicoes" />
                    </div>
                    
                </div>
            </div>
        )
    }
}