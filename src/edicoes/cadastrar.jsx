import React, {Component} from 'react'
import { hashHistory } from 'react-router' 
import Titulo from '../template/titulo'
import InputPadrao from '../template/input'
import AcoesForm from '../template/acoesForm'
import axios from 'axios'

const URL = 'http://localhost:8080/semana/'

export default class Cadastrar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            titulo: '',
            numero: ''
        }

        this.handleChangeNumero = this.handleChangeNumero.bind(this)
        this.handleChangeTitulo = this.handleChangeTitulo.bind(this)
    }

    handleChangeTitulo(e) {
        this.setState({ ...this.state, titulo: e.target.value })
    }

    handleChangeNumero(e) {
        this.setState({ ...this.state, numero: e.target.value })
    }

    handleAdd() {
        const lista = {
            titulo: this.state.titulo,
            numero: this.state.numero
        }
        axios.post(`${URL}edicaoController/apiAdd`, lista, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        }).then(res => {
            hashHistory.push("edicoes/editar/" + res.data[0].id)
        })
    }

    render() {
        return(
            <div className="content-wrapper">
                <div className="container-fluid">
                    <Titulo titulo="Edições" subtitulo="Cadastro" url="/edicoes" hideBtnNovo="true" />

                    <div className="row">
                        <InputPadrao label="Título" id="titulo" type="text" value={this.state.titulo} largura="col-md-12" handleChange={this.handleChangeTitulo} />
                        <InputPadrao label="Número" id="numero" type="text" value={this.state.numero} largura="col-md-6" handleChange={this.handleChangeNumero} />
                        <AcoesForm onClick={() => this.handleAdd()} voltar="/edicoes" />
                    </div>
                </div>
            </div> 
        )
    }
}