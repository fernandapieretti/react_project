import React from 'react'

export default props => (
    <div className={props.largura}>
        <div className="form-group">
            <label>{props.label}</label>
            <input className={props.class} id={props.id} name={props.id} type={props.type} value={props.value} onChange={props.handleChange} onClick={props.handleClick} />
        </div>
    </div>
)