import React from 'react'
import If from '../template/if'

export default props => (
  <If teste={props.handleExibir}>
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a className="navbar-brand" href="/">Semana On</a>
    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarResponsive">
      <ul className="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a className="nav-link" href="#/principal">
            <i className="fa fa-fw fa-dashboard"></i>
            <span className="nav-link-text">Dashboard</span>
          </a>
        </li>
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a className="nav-link" href="#/edicoes">
            <i className="fa fa-fw fa-area-chart"></i>
            <span className="nav-link-text">Edições</span>
          </a>
        </li>
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a className="nav-link" href="#/materias">
            <i className="fa fa-fw fa-area-chart"></i>
            <span className="nav-link-text">Matérias</span>
          </a>
        </li>
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i className="fa fa-fw fa-wrench"></i>
            <span className="nav-link-text">Categorias</span>
          </a>
          <ul className="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="navbar.html">Internas</a>
            </li>
            <li>
              <a href="cards.html">Externas</a>
            </li>
            <li>
              <a href="cards.html">Colunas</a>
            </li>
          </ul>
        </li>
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i className="fa fa-fw fa-file"></i>
            <span className="nav-link-text">Pessoas</span>
          </a>
          <ul className="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="login.html">Colunistas</a>
            </li>
            <li>
              <a href="register.html">Fotógrafos</a>
            </li>
          </ul>
        </li>
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i className="fa fa-fw fa-sitemap"></i>
            <span className="nav-link-text">Conteúdo</span>
          </a>
          <ul className="sidenav-second-level collapse" id="collapseMulti">
            <li>
              <a href="/">Anúncios</a>
            </li>
            <li>
              <a href="/">Enquetes</a>
            </li>
            <li>
              <a href="/">Textos</a>
            </li>
          </ul>
        </li>
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a className="nav-link" href="/">
            <i className="fa fa-fw fa-link"></i>
            <span className="nav-link-text">Semana On</span>
          </a>
        </li>
      </ul>
      <ul className="navbar-nav sidenav-toggler">
        <li className="nav-item">
          <a className="nav-link text-center" id="sidenavToggler">
            <i className="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <form className="form-inline my-2 my-lg-0 mr-lg-2">
            <div className="input-group">
              <input className="form-control" type="text" placeholder="Search for..." />
              <span className="input-group-append">
                <button className="btn btn-primary" type="button">
                  <i className="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="/" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i className="fa fa-fw fa-envelope"></i>
            <span className="d-lg-none">Messages
              <span className="badge badge-pill badge-primary">12 New</span>
            </span>
            <span className="indicator text-primary d-none d-lg-block">
              <i className="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div className="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 className="dropdown-header">New Messages:</h6>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="/">
              <strong>David Miller</strong>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="/">
              <strong>Jane Smith</strong>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="/">
              <strong>John Doe</strong>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item small" href="/">View all messages</a>
          </div>
        </li>
        <li className="nav-item">
          <a className="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i className="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
 </If> 
)