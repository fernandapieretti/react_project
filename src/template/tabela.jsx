import React, {Component} from 'react'
import Parser from 'html-react-parser'

export default class TabelaPadrao extends Component {

    toCols(headers) {
        const cols = headers ? headers.split(', ') : [] 
        let titulos = ''
        cols.map(function(c) {
            titulos += "<th>" + c + "</th>"
        })

        return Parser(titulos)
    }

    render() {
        return (
            <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                <thead>
                    <tr>
                        {this.toCols(this.props.headers)}
                    </tr>
                </thead>
                {this.props.children}
            </table>
        )
    }
}