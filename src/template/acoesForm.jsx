import React from 'react'
import { Link } from 'react-router'

export default props => (
    <div className="col-md-12">
        <button className="btn btn-success" onClick={props.onClick}>Salvar</button>
        <Link className="btn" to={props.voltar}>Voltar</Link>
    </div>
)