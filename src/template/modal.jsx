import React, {Component} from 'react'

export default class Modal extends Component {

    modalClass(show) {
        let classes = 'modal fade'
        if (show)
            classes += ' show'
        return classes
    }

    modalBackdropClass(show) {
        let classes = 'modal-backdrop fade'
        if (show)
            classes += ' show'
        return classes
    }

    render() {
        return (
            <div>
                <div className={this.modalClass(this.props.show)} tabIndex="-1" role="dialog" >
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">{this.props.titulo}</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" onClick={this.props.handleCancel}>&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <p>{this.props.texto}</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" onClick={this.props.handleConfirm}>Ok</button>
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.props.handleCancel}>Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={this.modalBackdropClass(this.props.show)}></div>
            </div>
        )
    }
}