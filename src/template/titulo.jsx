import React, {Component} from 'react'
import IconButton from './iconButton'
import If from './if'

export default props => (
    <div className="row">
        <div className="col-md-12">
            <ol className="breadcrumb">
                <li className="breadcrumb-item">
                    <a href={"#/" + props.url}>{props.titulo}</a>
                </li>
                <li className="breadcrumb-item active">{props.subtitulo}</li>
                <If teste={!props.hideBtnNovo}>
                    <li><a href={"#/" + props.url + "/novo"}>Novo</a></li>
                </If>
            </ol>
        </div>
    </div>
)