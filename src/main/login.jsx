import React, {Component} from 'react'
import { hashHistory } from 'react-router'
import Input from '../template/input'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.logIn = this.logIn.bind(this)
    }

    logIn() {
        localStorage.setItem("loggedIn999", true)
        hashHistory.push("/")
        console.log('teste')
    }

    render() {
        return(
            <div className="container">
                <div className="card card-login mx-auto mt-5">
                    <div className="card-header">Login</div>
                    <div className="card-body">
                        <Input label="Usuário" id="login" type="text" value="" class="form-control" handleChange="" />
                        <Input label="Senha" id="senha" type="password" value="" class="form-control" handleChange="" />
                        <Input id="enviar" type="button" value="Logar" class="btn btn-primary btn-block" handleClick={() => this.logIn()} />
                    </div>
                </div>
            </div>
        )
    }
}