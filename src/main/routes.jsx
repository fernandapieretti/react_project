import React, {Component} from 'react'
import {Router, Route, Redirect, hashHistory} from 'react-router'

import Login from '../main/login'
import Principal from '../principal/principal'
import Materias from '../materias/lista'
import MateriasEditar from '../materias/editar'
import MateriasCadastrar from '../materias/cadastrar'
import Edicoes from '../edicoes/lista'
import EdicoesEditar from '../edicoes/editar'
import EdicoesCadastrar from '../edicoes/cadastrar'

const login = localStorage.loggedIn999

export default class Routes extends Component {

    requireAuth() {
        if (!login) {
            hashHistory.push("login")
        }
    }

    checkAuth() {
        if (login) {
            hashHistory.push("principal")
        }
    }
    
    render() {
        return(
            <Router history={hashHistory}>
                <Route path="/login" component={Login} onEnter={this.checkAuth} />
                <Route path="/principal" component={Principal} onEnter={this.requireAuth} />
                <Route path="/materias" component={Materias} onEnter={this.requireAuth} />
                <Route path="/materias/novo" component={MateriasCadastrar} onEnter={this.requireAuth} />
                <Route path="/materias/editar/:id" component={MateriasEditar} onEnter={this.requireAuth} />
                <Route path="/edicoes" component={Edicoes} onEnter={this.requireAuth} />
                <Route path="/edicoes/novo" component={EdicoesCadastrar} onEnter={this.requireAuth} />
                <Route path="/edicoes/editar/:id" component={EdicoesEditar} onEnter={this.requireAuth} />
                <Redirect from="*" to="/principal" />
            </Router>
        )
    }
}