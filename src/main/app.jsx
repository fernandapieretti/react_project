import 'modules/bootstrap/dist/css/bootstrap.min.css'
import 'modules/font-awesome/css/font-awesome.min.css'
import '../css/sb-admin.min.css'

import React, {Component} from 'react'
import Routes from './routes'
import Menu from '../template/menu'

const login = localStorage.loggedIn999

export default class App extends Component {
    render() {
        return (
            <div>
                <Menu handleExibir={login} />
                <Routes />
            </div>
        )
    }
}