import React, {Component} from 'react'
import Titulo from '../template/titulo'

export default class Principal extends Component {
    render() {
        return (
            <div className="content-wrapper">
                <div className="container-fluid">
                    <Titulo titulo="Dashboard" subtitulo="My Dashboard" url="principal" hideBtnNovo="true" />
                </div>
            </div>
        )
    }
}