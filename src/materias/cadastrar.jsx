import React, {Component} from 'react'
import { hashHistory } from 'react-router'
import Titulo from '../template/titulo'
import InputPadrao from '../template/input'
import AcoesForm from '../template/acoesForm'
import axios from 'axios'

const URL = "http://localhost:8080/semana/"

export default class Cadastrar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            titulo: '',
            idcategoria: '',
            data: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleChangeData = this.handleChangeData.bind(this)
        this.handleChangeCat = this.handleChangeCat.bind(this)
    }

    handleAdd() {
        const lista = {
            titulo : this.state.titulo,
            idcategoria : this.state.idcategoria,
            data : this.state.data
        }
        
        axios.post(`${URL}conteudoController/apiAddConteudo`, lista, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        }).then(res => {
            hashHistory.push("/materias/editar/" + res.data[0].id)
        })
    }

    handleChange(e) {
        this.setState({ ...this.state, titulo: e.target.value })
    }

    handleChangeData(e) {
        this.setState({ ...this.state, data: e.target.value })
    }

    handleChangeCat(e) {
        this.setState({ ...this.state, idcategoria: e.target.value })
    }

    render() {
        return (
            <div className="content-wrapper">
                <div className="container-fluid">
                    <Titulo titulo="Matérias" subtitulo="Cadastro" url="materias" hideBtnNovo="true" />
                    
                    <div className="row">
                        <InputPadrao label="Título" id="titulo" type="text" value={this.state.titulo} largura="col-md-12" handleChange={this.handleChange} />
                        <InputPadrao label="Data" id="data" type="date" value={this.state.data} largura="col-md-3" handleChange={this.handleChangeData}  />
                        <InputPadrao label="Categoria" id="idcategoria" type="text" value={this.state.idcategoria} largura="col-md-3" handleChange={this.handleChangeCat} />
                        <AcoesForm onClick={() => this.handleAdd()} voltar="/materias" />
                    </div>
                </div>
            </div>
        )
    }

}