import React, {Component} from 'react'
import { hashHistory } from 'react-router';
import Titulo from '../template/titulo'
import TabelaPadrao from '../template/tabela'
import IconButton from '../template/iconButton'
import Modal from '../template/modal'
import axios from 'axios'

const URL = "http://localhost:8080/semana/"

export default class Lista extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lista: [],
            show: false,
            id: ''
        }
        this.handleConfirmRemove = this.handleConfirmRemove.bind(this)        
        this.handleRemove = this.handleRemove.bind(this)        
        this.handleCancel = this.handleCancel.bind(this)        
        this.handleEdit = this.handleEdit.bind(this)        
    }

    componentDidMount() {
        this.refresh()
    }

    refresh() {
        axios.get(`${URL}conteudoController/api`)
        .then(res => this.setState({...this.state, lista: res.data }))
    }

    handleEdit(materia) {
        hashHistory.push("/materias/editar/" + materia);
    }

    handleConfirmRemove(materia) {
        this.setState({ ...this.state, show: true, id: materia })
    }

    handleCancel() {
        this.setState({ ...this.state, show: false, id: '' })
    }

    handleRemove() {
        const id = {
            id: this.state.id
        }
        axios.post(`${URL}conteudoController/apiDeleteConteudo`, id, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        }).then(resp => { 
            this.refresh()
            this.handleCancel()
         })
    }

    render() {
        return (
            <div className="content-wrapper">
                <div className="container-fluid">
                    <Titulo titulo="Matérias" subtitulo="Listagem" url="materias" />

                    <TabelaPadrao headers="Id, Título, Categoria, Ações">
                        <tbody>
                        {
                            this.state.lista.map((materia, index) => {
                                return (
                                    <tr key={materia.id}>
                                        <td>{materia.id}</td>
                                        <td>{materia.titulo}</td>
                                        <td>{materia.categoria}</td>
                                        <td>
                                            <IconButton type="success" icon="pencil" onClick={() => this.handleEdit(materia.id)} />
                                            <IconButton type="danger" icon="trash" onClick={() => this.handleConfirmRemove(materia.id)} />
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </TabelaPadrao>
                    
                    <Modal show={this.state.show}
                    titulo="Confirmação" texto="Deseja excluir esta matéria? Esta ação não poderá ser desfeita." 
                    handleCancel={() => this.handleCancel()} 
                    handleConfirm={() => this.handleRemove()} />
                </div>
            </div>
        )
    }

}