import React, {Component} from 'react'
import { hashHistory } from 'react-router';
import Titulo from '../template/titulo'
import InputPadrao from '../template/input'
import AcoesForm from '../template/acoesForm'
import axios from 'axios'

const URL = "http://localhost:8080/semana/"

export default class Editar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.params.id,
            titulo: '',
            idcategoria: '',
            data: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleChangeData = this.handleChangeData.bind(this)
        this.handleChangeCat = this.handleChangeCat.bind(this)
    }

    componentDidMount() {
        axios.get(`${URL}conteudoController/apiBuscarPorId/${this.state.id}`)
        .then(res => {
            const materia = res.data;
            this.setState({
                titulo: materia[0].titulo,
                data: materia[0].data,
                idcategoria: materia[0].idcategoria
            });
        });
    }

    handleEdit() {
        const materia = {
            id: this.state.id,
            titulo: this.state.titulo,
            data: this.state.data,
            idcategoria: this.state.idcategoria
        }
        axios.post(`${URL}conteudoController/apiEditarConteudo`, materia, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
        }).then(res => {            
            hashHistory.push("/materias");
        })
    }

    handleChange(e) {
        this.setState({ ...this.state, titulo: e.target.value })
    }

    handleChangeData(e) {
        this.setState({ ...this.state, data: e.target.value })
    }

    handleChangeCat(e) {
        this.setState({ ...this.state, idcategoria: e.target.value })
    }

    render() {
        return (
            <div className="content-wrapper">
                <div className="container-fluid">
                    <Titulo titulo="Matérias" subtitulo="Editar" url="materias" hideBtnNovo="true" />

                    <div className="row">
                        <InputPadrao label="Título" id="titulo" type="text" value={this.state.titulo} largura="col-md-12" handleChange={this.handleChange} />
                        <InputPadrao label="Data" id="data" type="date" value={this.state.data} largura="col-md-3" handleChange={this.handleChangeData}  />
                        <InputPadrao label="Categoria" id="idcategoria" type="text" value={this.state.idcategoria} largura="col-md-3" handleChange={this.handleChangeCat} />
                        <AcoesForm onClick={() => this.handleEdit()} voltar="/materias" />
                    </div>
                    
                </div>
            </div>
        )
    }

}